import { Schema } from "mongoose";

let classroomSchema= Schema({
    name:{
        required:true,
        type: String,
    },
    noOfBenches:{
        required:true,
        type:Number,
    },
    hasTv:{
        required:false,
        type:Boolean,
    }

})
export default classroomSchema