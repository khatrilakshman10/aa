import { Router, json } from "express";
import { Teacher } from "../schema/model.js";

let teacherRouter=Router()
teacherRouter.route("/").post((req,res,next)=>{
    let data=req.body
    Teacher.create(data)
    res.json({
        success:true,
        message:"Teacher create successfully"
    })
})
export default teacherRouter