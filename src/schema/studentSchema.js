import { Schema } from "mongoose";
let studentSchema= Schema({
    name:{
        required:true,
        type: String,
    },
    age:{
        required:true,
        type:Number,
    },
    isMarried:{
        required:false,
        type:Boolean,
    }

})
export default studentSchema