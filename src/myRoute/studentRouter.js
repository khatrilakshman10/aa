import { Router } from "express";
import { Student } from "../schema/model.js";

let studentRouter=Router()
studentRouter.route("/").post((req,res,next)=>{
    let data = req.body
    Student.create(data)
    res.json({
        success:true,
        message:"Student created successfully"
    })
})
export default studentRouter