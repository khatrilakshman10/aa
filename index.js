import express, { json } from "express";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { firstRouter } from "./src/route/firstRouter.js";
import { nameRouter } from "./src/route/nameRouter.js";
import { bikeRouter } from "./src/route/bikeRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongoDb.js";
import studentRouter from "./src/myRoute/studentRouter.js";
import bookRouter from "./src/myRoute/bookRouter.js";
import departmentRouter from "./src/myRoute/departmentRouter.js";
import classroomRouter from "./src/myRoute/classroomRouter.js";
import collegeRouter from "./src/myRoute/collegeRouter.js";
import traineeRouter from "./src/myRoute/traineeRouter.js";
import teacherRouter from "./src/myRoute/teacherRouter.js";

let expressApp = express();
connectToMongoDb()

// expressApp.use((req,res,next)=>{
//   console.log("i am application,normal middleware")
//   let error=new Error("i am application error")
//   next(error)
// },
// (error,res,req,next)=>{
//   console.log("i am application,error middleware 1")
//   console.log(error.message)
//   next()

// },
// (req,res,next)=>{
//   console.log("i am application,normal middleware 2 ")
//   next()
// })
expressApp.use(json());//it is done to make our application to accept json data

//expressApp.use("/trainees", traineesRouter);
expressApp.use("/",firstRouter)
expressApp.use("/names",nameRouter) 
expressApp.use("/bikes",bikeRouter) 
expressApp.use("/students", studentRouter)
expressApp.use("/books",bookRouter)
expressApp.use("/teachers",teacherRouter)
expressApp.use("/departments",departmentRouter)
expressApp.use("/classrooms",classroomRouter)
expressApp.use("/colleges",collegeRouter)
expressApp.use("/trainees",traineeRouter)
expressApp.listen(8000, () => {
  console.log("app is listening at port 8000");
});
