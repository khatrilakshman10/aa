
import studentSchema from "./studentSchema.js";
export let Student = model("Student",studentSchema)

import teacherSchema from "./teacherSchema.js";
export let Teacher = model("Teacher",teacherSchema)

import bookSchema from "./bookSchema.js";
export let Book = model("Book", bookSchema)

import traineeSchema from "./traineeSchema.js";
export let Trainee=model("Trainee",traineeSchema)

import collegeSchema from "./collegeSchema.js";
export let College=model("College",collegeSchema)

import classroomSchema from "./classroomSchema.js";
export let Classroom=model("Classroom",classroomSchema)

import departmentSchema from "./departmentSchema.js";
import { model } from "mongoose";
export let Department= model("Department",departmentSchema)