import { Router } from "express";
import { Department } from "../schema/model.js";

let departmentRouter=Router()
departmentRouter.route("/").post((req,res,next)=>{
    let data=req.body
    Department.create(data)
    res.json({
        success:true,
        message:"Department create successfully"
    })
})
export default departmentRouter